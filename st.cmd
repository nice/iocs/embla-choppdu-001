require (rittalpduman2)
# -----------------------------------------------------
# Configure Environment
# -----------------------------------------------------
# Default paths to locate database and protocol
#epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(chooperpdu_DIR)db/")
epicsEnvSet("IP_ADDRESS","172.30.235.16:502")
epicsEnvSet("PORT", "502")
epicsEnvSet("PREFIX", "P:")
epicsEnvSet("P", "LabS-EMBLA:")
epicsEnvSet("R", "Chop-PDU-01:")
# ------------------------------------------------------------
# Asyn IP Configuration
# ------------------------------------------------------------
#drvAsynIPPortConfigure(portName, hostInfo, priority, noAutoConnect, noProcessEos)
drvAsynIPPortConfigure("Chopper", "$(IP_ADDRESS)", 0,0,1)
# -------------------------------------------------------------
# Modbus configuration
# --------------------------------------------------------------
# modbusInterposeConfig(portName,
#                       linkType,
#                       timeoutMsec,
#                       writeDelayMsec)
modbusInterposeConfig("Chopper",0, 1000,0)
# ---------------------------------------------------------------
# NOTE: We use octal numbers for the start address and length (leading zeros)
# #       to be consistent with the PLC nomenclature.  This is optional, decimal
# #       numbers (no leading zero) or hex numbers can also be used.
#
#drvModbusAsynConfigure(portName, 
#                       tcpPortName,
#                       slaveAddress, 
#                       modbusFunction, 
#                       modbusStartAddress, 
#                       modbusLength,
#                       dataType,
#                       pollMsec, 
#                       plcType);
##
# -----------------------------------------------------------------------------
#  InputRegisterPort
#  InputRegister	0	0	0x0000	11	system identification	RITTAL CMCIII PDU
#  InputRegister	0	11	0x000B	3	mac address	00:26:3C:00:B0:C5
#-----------------------------------------------------------------------------
#
 drvModbusAsynConfigure("PDU_Input", "Chopper", 1, 4, 0000, 37, 0, 100,"MODBUS_DATA")
#drvModbusAsynConfigure("PDU_InputM", "Chopper", 0, 4,  0000, 03, 0, 100,"MODBUS_DATA")
#drvModbusAsynConfigure("PDU_IRCur",  "Chopper", 1, 4, 27408, 02, 0, 100,"")
#
drvModbusAsynConfigure("IRReadPort", "Chopper", 1, 4, 27408, 10, 0, 1000,"")
#
drvModbusAsynConfigure("IR_InWord", "Chopper", 1, 4, 27408, 10, 0, 1000,"IRReadPort")

drvModbusAsynConfigure("IRReadL1C", "Chopper", 1, 4, 27408, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadL1P", "Chopper", 1, 4, 27552, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadL2C", "Chopper", 1, 4, 28128, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadL2P", "Chopper", 1, 4, 28272, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadL3C", "Chopper", 1, 4, 28848, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadL3P", "Chopper", 1, 4, 28992, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS01C", "Chopper", 1, 4, 29568, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS01P", "Chopper", 1, 4, 29712, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS02C", "Chopper", 1, 4, 30096, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS02P", "Chopper", 1, 4, 30240, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS03C", "Chopper", 1, 4, 30624, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS03P", "Chopper", 1, 4, 30768, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS04C", "Chopper", 1, 4, 31152, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS04P", "Chopper", 1, 4, 31296, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS05C", "Chopper", 1, 4, 31680, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS05P", "Chopper", 1, 4, 31824, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS06C", "Chopper", 1, 4, 32208, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS06P", "Chopper", 1, 4, 32352, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS07C", "Chopper", 1, 4, 32736, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS07P", "Chopper", 1, 4, 32880, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS08C", "Chopper", 1, 4, 33264, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS08P", "Chopper", 1, 4, 33408, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS09C", "Chopper", 1, 4, 33792, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS09P", "Chopper", 1, 4, 33936, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS10C", "Chopper", 1, 4, 34320, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS10P", "Chopper", 1, 4, 34464, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS11C", "Chopper", 1, 4, 34848, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS11P", "Chopper", 1, 4, 34992, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS12C", "Chopper", 1, 4, 35376, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS12P", "Chopper", 1, 4, 35520, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS13C", "Chopper", 1, 4, 35904, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS13P", "Chopper", 1, 4, 36048, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS14C", "Chopper", 1, 4, 36432, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS14P", "Chopper", 1, 4, 36576, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS15C", "Chopper", 1, 4, 36960, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS15P", "Chopper", 1, 4, 37104, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS16C", "Chopper", 1, 4, 37488, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS16P", "Chopper", 1, 4, 37632, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS17C", "Chopper", 1, 4, 38016, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS17P", "Chopper", 1, 4, 38160, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS18C", "Chopper", 1, 4, 38544, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS18P", "Chopper", 1, 4, 38688, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS19C", "Chopper", 1, 4, 39072, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS19P", "Chopper", 1, 4, 39216, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS20C", "Chopper", 1, 4, 39600, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS20P", "Chopper", 1, 4, 39744, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS21C", "Chopper", 1, 4, 40128, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS21P", "Chopper", 1, 4, 40272, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS22C", "Chopper", 1, 4, 40656, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS22P", "Chopper", 1, 4, 40800, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS23C", "Chopper", 1, 4, 41184, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS23P", "Chopper", 1, 4, 41328, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS24C", "Chopper", 1, 4, 41712, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS24P", "Chopper", 1, 4, 41856, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS25C", "Chopper", 1, 4, 42240, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS25P", "Chopper", 1, 4, 42384, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS26C", "Chopper", 1, 4, 42768, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS26P", "Chopper", 1, 4, 42912, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS27C", "Chopper", 1, 4, 43296, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS27P", "Chopper", 1, 4, 43440, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS28C", "Chopper", 1, 4, 43824, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS28P", "Chopper", 1, 4, 43968, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS29C", "Chopper", 1, 4, 44352, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS29P", "Chopper", 1, 4, 44496, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS30C", "Chopper", 1, 4, 44880, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS30P", "Chopper", 1, 4, 45024, 02, 0, 1000,"IRReadPort")

# -----------------------------------------------------------------------------
# e3 Common databases, autosave, etc.
# -----------------------------------------------------------------------------
# E3 Common databases
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

## Load record instances
#dbLoadTemplate("pdu.substitutions")
dbLoadRecords("aiRead.db","P=$(P), R=$(R)")
dbLoadRecords("calc.db","P=$(P), R=$(R)")
dbLoadRecords("sum.db","P=$(P), R=$(R)")

# -----------------------------------------------------------------------------
# Debug
# -----------------------------------------------------------------------------
#asynSetTraceIOMask("Chopper", 0, 4)
#asynSetTraceIOTruncateSize("Chopper", 0, 512)
#dbLoadTemplate("abb.substitutions")
#iocinit()


